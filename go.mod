module gitlab.com/PUSOFTDEV/gokil2

go 1.20

require (
	github.com/OneOfOne/cmap v0.0.0-20170825200327-ccaef7657ab8
	github.com/aerospike/aerospike-client-go v4.5.2+incompatible
	github.com/davecgh/go-spew v1.1.1
	github.com/fatih/color v1.15.0
	github.com/go-sql-driver/mysql v1.7.1
	github.com/jmoiron/sqlx v1.3.5
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible
	github.com/julienschmidt/httprouter v1.3.0
	github.com/kr/pretty v0.3.1
	github.com/lib/pq v1.10.9
	github.com/mutecomm/go-sqlcipher v0.0.0-20190227152316-55dbde17881f
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/satori/go.uuid v1.2.0
	github.com/streamrail/concurrent-map v0.0.0-20160823150647-8bf1e9bacbf6
	github.com/tdewolff/minify v2.3.6+incompatible
	github.com/urfave/cli v1.22.14
	github.com/yosuke-furukawa/json5 v0.1.1
	golang.org/x/crypto v0.11.0
	gopkg.in/redis.v5 v5.2.9
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.8 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	github.com/tdewolff/test v1.0.9 // indirect
	github.com/yuin/gopher-lua v1.1.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
)

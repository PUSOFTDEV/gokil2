package D

import (
	"time"
)

// records 5W+1H for one login session
/*
 * Specification for the log format:
 * 1. RecordName.get(id) | Date | Time | fields modified (restore command)
 */
type AccessLog struct {
	Id        int64
	UserId    int64
	LogCount  int64
	Session   string
	IpAddress string
	Logs      string // modified records
}

// records 5W+1H for one record
/*
 * Specification for the log format:
 * 1. User.get(id) | IP Address | Session | Time | fields modified (restore command)
 */
type RecordLog struct {
	Id         int64
	RecordId   int64
	SessionId  int64
	Date       time.Time // Date() only as key
	Info       string
	DataBefore string
	DataAfter  string
}

/*

// identify this base
func (b Base) Identify() string {
	return b.Table + `:` + Int.ToS(b.Id)
}

// fetch one record from table
// TODO: test
func (b *Base) GetById(table string, id int64) {
	b.Id = id
	query := `SELECT * FROM ` + ZZ(table) + ` WHERE id = ` + Int.ToS(id)
	b.First(query, table)
}

// fetch one by unique-id
func (b *Base) GetByUniq(table, uid string) {
	b.UniqueId = uid
	query := `SELECT * FROM ` + ZZ(table) + ` WHERE unique_id = ` + Z(uid)
	b.First(query, table)
}

// check if exists
func (b *Base) IdExists(table string, id int64) bool {
	query := `SELECT COUNT(*) FROM ` + table + ` WHERE id = ` + Int.ToS(id)
	return PG.QInt(query) == 1
}

// check if unique-id exists
func (b *Base) UniqExists(table, uid string) bool {
	query := `SELECT COUNT(*) FROM ` + table + ` WHERE unique_id = ` + Z(uid)
	return PG.QInt(query) == 1
}

// fetch one record from table by query, second parameter must be table name
// TODO: test
func (b *Base) First(query string, table string, params ...interface{}) {
	b.Table = table
	PG.Adapter.Get(b, query, params...)
}

// save one record from table by jquery
// TODO: test
func (b *Base) Save(ctx *web.Context) {
	// TODO: refactor this
	if b.Table == `` {
		Log.Panic(errors.New(`missing table name`), `base table name must not be empty`)
	}

	PG.DoTransaction(func(tx *Transaction) {

		now := time.Now()
		actor_id := ctx.Session.UserId
		table_name := b.Table
		id := b.Id
		if id == 0 { // INSERT
			values := K.M{
				`unique_id`:  nil,
				`created_at`: now,
				`updated_at`: now,
				`created_by`: actor_id,
				`data`:       b.Data,
			}
			if len(b.UniqueId) > 0 {
				values[`unique_id`] = b.UniqueId
			}
			id = tx.DoInsert(table_name, values)
			b.Id = id

			// // TODO: create or insert RecordLog
			// rlog := fmt.Sprintf(`%d. users[%d] created %s[%d]\n`, 1, actor_id, table_name, id)
			// rec_vals := K.M{
			// 	`record_id`: id,
			// 	`date`:      now,
			// 	`log_count`: 1,
			// 	`logs`:      rlog,
			// }
			// rec_id := PG.DoInsert(`recod_log_`+table_name, rec_vals)

			// // TODO: create or insert SessionLog

			// slog := Int.ToS(...?)

		} else { // UPDATE

			// // TODO: create or insert SessionLog
			// b.UpdatedAt = now
			// b.UpdatedBy = ctx.Session.UserId
			// if b.CreatedAt.Equal(time.Time{}) {
			// 	b.CreatedAt = now
			// 	b.CreatedBy = actor_id
			// }
		}
	})

}

// convert base data to K.M
func (b *Base) ParseData() K.M {
	var data K.M
	err := json.Unmarshal([]byte(b.Data), data)
	Log.Panic(err, `failed to parse data `+b.Identify())
	return data
}
*/

// func main() {
//     // create connection to the clients_test database
//     db, err := sql.Open(`mysql`, `mysqlAddress`)
//     if err != nil {
//         panic(err)
//     }
//     defer db.Close()

//     rows, err := db.Query(`SELECT name,age FROM users WHERE age>=50`)
//     if err != nil {
//         log.Fatal(err)
//     }

//     values := PackageData(rows)

//     for i := range values {
//         for k, v := range values[i] {
//             fmt.Println(k, `:`, v)
//         }
//     }
// }

// func PackageData(rows *sql.Rows) []map[string]string {
//     columns, err := rows.Columns()
//     if err != nil {
//         fmt.Println(`Unable to read from database`, err)
//         log.Println(err)
//         return nil
//     }
//     if len(columns) == 0 {
//         return nil
//     }

//     values := make([]sql.RawBytes, len(columns))

//     scanArgs := make([]interface{}, len(values))
//     for i := range values {
//         scanArgs[i] = &values[i]
//     }

//     data := make([]map[string]string, len(values))

//     // Fetch rows
//     for rows.Next() {
//         newRow := make(map[string]string)
//         // get RawBytes from data
//         err := rows.Scan(scanArgs...)
//         if err != nil {
//             fmt.Println(`Unable to read from database`, err)
//             log.Println(err)
//             rows.Close()
//             return nil
//         }
//         var value string
//         for i, col := range values {
//             if col == nil {
//                 value = `NULL`
//             } else {
//                 value = string(col)
//             }
//             newRow[columns[i]] = value
//         }
//         data = append(data, newRow)
//     }
//     rows.Close()
//     return data
// }

////
// import `fmt`

// func main() {
// 	// Execute the query
// 	rows, err := db.Query(`SELECT * FROM table`)
// 	if err != nil {
// 		panic(err.Error())
// 	}

// 	// Get column names
// 	columns, err := rows.Columns()
// 	if err != nil {
// 		panic(err.Error())
// 	}

// 	// Make a slice for the values
// 	values := make([]interface{}, len(columns))

// 	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
// 	// references into such a slice
// 	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
// 	scanArgs := make([]interface{}, len(values))
// 	for i := range values {
// 		scanArgs[i] = &values[i]
// 	}

// 	// Fetch rows
// 	for rows.Next() {
// 		err = rows.Scan(scanArgs...)
// 		if err != nil {
// 			panic(err.Error())
// 		}

// 		// Print data
// 		for i, value := range values {
// 			switch value.(type) {
// 			case nil:
// 				fmt.Println(columns[i], `: NULL`)

// 			case []byte:
// 				fmt.Println(columns[i], `: `, string(value.([]byte)))

// 			default:
// 				fmt.Println(columns[i], `: `, value)
// 			}
// 		}
// 		fmt.Println(`-----------------------------------`)
// 	}
//  rows.Close()
// }

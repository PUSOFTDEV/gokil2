GoKIL: Go Keen (for) Improvement Library

- A = Array/Slice
- B = Boolean
- C = Character
- D = Database
- F = Floating Point
- I = Integer
- J = JSON
- K = Misc
- L = logging
- M = Map/Hash
- S = String
- T = Time
- W = Web Framework (depends on Redis for sessions)
- X = Any/interface{}
- Z = Z-Template Engine (javascript-friendly)

See _example directory on how to use the web framework.

TODO: 
- replace [httprouter](https://github.com/julienschmidt/httprouter) with [fasthttprouter](https://github.com/buaazp/fasthttprouter) 
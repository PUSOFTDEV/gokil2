package main

import (
	"os"

	_ "gitlab.com/PUSOFTDEV/gokil2/A" // array
	_ "gitlab.com/PUSOFTDEV/gokil2/B" // bool
	_ "gitlab.com/PUSOFTDEV/gokil2/C" // character
	_ "gitlab.com/PUSOFTDEV/gokil2/D" // database
	_ "gitlab.com/PUSOFTDEV/gokil2/F" // float
	_ "gitlab.com/PUSOFTDEV/gokil2/I" // integer
	_ "gitlab.com/PUSOFTDEV/gokil2/J" // json
	_ "gitlab.com/PUSOFTDEV/gokil2/K" // others
	_ "gitlab.com/PUSOFTDEV/gokil2/L" // logs
	_ "gitlab.com/PUSOFTDEV/gokil2/M" // map
	_ "gitlab.com/PUSOFTDEV/gokil2/S" // string
	_ "gitlab.com/PUSOFTDEV/gokil2/T" // time
	_ "gitlab.com/PUSOFTDEV/gokil2/W" // web
	_ "gitlab.com/PUSOFTDEV/gokil2/X" // anything
	_ "gitlab.com/PUSOFTDEV/gokil2/Z" // z-template engine

	// `github.com/kokizzu/gin`
	// `github.com/kokizzu/colorgo`
	// `github.com/pwaller/goupx`
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = `gokil`
	app.Usage = `GO Keen for Improvement Library`
	app.Commands = []cli.Command{
		{
			Name:    `init`,
			Aliases: []string{`i`},
			Usage:   `initialize project`,
			Action: func(c *cli.Context) {
				os.Mkdir(`ajax`, 0750)
				os.Mkdir(`model`, 0750)
				os.Mkdir(`page`, 0750)
				os.MkdirAll(`public/js`, 0750)
				os.MkdirAll(`public/css`, 0750)
				os.Mkdir(`views`, 0750)
			},
		},
	}

	app.Run(os.Args)
}
